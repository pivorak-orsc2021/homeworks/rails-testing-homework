class Todo < ApplicationRecord
  validates_presence_of :title, :due_date

  scope :active, -> { where(status: false) }
  scope :completed, -> { where(status: true) }

  def can_destroy?
    !overdue?
  end

  def complete
    self.status = true
    GreetingsLogger.call
  end

  def activate
    self.status = false
  end

  private

  def overdue?
    due_date < Date.today
  end
end
